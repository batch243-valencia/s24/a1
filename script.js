// console.log("Hello");

// In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
// Link the script.js file to the index.html file.

// Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
let base = 2; 
let getCube = (Math.pow(base,3));
// Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
console.log(`the cube of ${base} is ${getCube}`);

// Create a variable address with a value of an array containing details of an address.
const address = ["258 Washington Ave Nw","California", "90011"];
// Destructure the array and print out a message with the full address using Template Literals.
const [street,state,postal] = address;
console.log(`i live at ${street}, ${state} ${postal}`);
// Create a variable animal with a value of an object data type with different animal details as its properties.
// name
// species
// weight
// measurement
const animal = {
    name:'Elliot',
    species:'Feline',
    weight:'3.4kg',
    measurement:'14 inches'
}
// Destructure the object and print out a message with the details of the animal using Template Literals.
const {name,species,weight,measurement}=animal
console.log(`${name} is a ${species}. He weighted at ${weight} with a measurement of ${measurement}.`);

// Create an array of numbers.
// Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

const numbers =[1,2,3,4,5];
numbers.forEach((numbers)=>{console.log(`${numbers}`)})
//  Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
const reduceNumber = numbers.reduce((x, y) => x + y);
console.log(reduceNumber);
// Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
class Dog {
  constructor(name,age,breed) {
    this.name=name;
    this.age=age;
    this.year=breed;
  }
}
// Create/instantiate a new object from the class Dog and console log the object.
const booby =  new Dog("Bobby","8months","2022");

console.log(booby);
// Create a git repository named S24.
// Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// Add the link in Boodle.